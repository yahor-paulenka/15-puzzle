using FifteenPuzzle.Level;
using System.Collections.Generic;
using UnityEngine;


namespace FifteenPuzzle.Services.Assets
{
    public class LevelAssetService : AssetServiceBase
    {
        public LevelAssetService(PrefabPool prefabPool) : base(prefabPool) { }


        public LevelController InstantiateLevelController(int fieldSize, Transform parent)
        {
            GameObject gameObject = new GameObject(nameof(LevelController));
            gameObject.transform.SetParent(parent);
            gameObject.transform.localPosition = Vector3.zero;

            LevelController levelController = gameObject.AddComponent<LevelController>();
            levelController.Initialize(fieldSize);
            return levelController;
        }


        public List<Cell> CreatCells(int count, string path, Transform parent)
        {
            List<Cell> cells = new List<Cell>(count);
            for (int i = 0; i < count; i++)
            {
                cells.Add(Instantiate<Cell>(path, parent));
            }

            return cells;
        }
    }
}
