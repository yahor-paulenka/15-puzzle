using System.Collections.Generic;
using UnityEngine;


namespace FifteenPuzzle.Services.Assets
{
    public class PrefabPool
    {
        private readonly Dictionary<string, GameObject> pool = new Dictionary<string, GameObject>();


        public bool TryGet(string path, out GameObject prefab)
        {
            if (!pool.ContainsKey(path))
            {
                prefab = null;
                return false;
            }

            prefab = pool[path];
            return true;
        }


        public bool TryAdd(string path, GameObject prefab)
        {
            if (pool.ContainsKey(path))
                return false;


            pool.Add(path, prefab);
            return true;
        }


        public void UnloadAll()
        {
            foreach (var p in pool)
            {
                Resources.UnloadAsset(p.Value);
            }

            pool.Clear();
        }
    }
}
