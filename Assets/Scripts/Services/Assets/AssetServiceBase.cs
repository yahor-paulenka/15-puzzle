using System;
using UnityEngine;
using Object = UnityEngine.Object;


namespace FifteenPuzzle.Services.Assets
{
    public class AssetServiceBase
    {
        #region Fields

        private readonly PrefabPool pool;

        #endregion



        #region Class lifecycle

        protected AssetServiceBase(PrefabPool pool = null)
        {
            this.pool = pool;
        }

        #endregion



        #region Protected methods

        protected T Instantiate<T>(string path, Transform parent) where T : Component
        {
            GameObject prefab = LoadPrefab(path);
            return Instantiate<T>(prefab, parent);
        }


        protected GameObject Instantiate(string path, Transform parent)
        {
            GameObject prefab = LoadPrefab(path);
            return Instantiate(prefab, parent);
        }

        #endregion



        #region Private methods

        private GameObject Instantiate(GameObject prefab, Transform parent) => Object.Instantiate(prefab, parent);


        private T Instantiate<T>(GameObject prefab, Transform parent) where T : Component
        {
            GameObject copy = Instantiate(prefab, parent);
            T component = copy.GetComponent<T>();
            if (component == null)
            {
                throw new InvalidOperationException();
            }

            return component;
        }


        private GameObject LoadPrefab(string path)
        {
            if (pool != null && pool.TryGet(path, out GameObject storedPrefab))
            {
                return storedPrefab;
            }

            GameObject prefab = Resources.Load<GameObject>(path);
            if (pool != null)
            {
                pool.TryAdd(path, prefab);
            }

            return prefab;
        }

        #endregion
    }
}
