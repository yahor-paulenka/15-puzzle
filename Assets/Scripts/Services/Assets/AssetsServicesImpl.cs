namespace FifteenPuzzle.Services.Assets
{
    public class AssetsServicesImpl : IAssetsServices
    {
        #region Class lifecycle

        public AssetsServicesImpl()
        {
            PrefabPool prefabPool = new PrefabPool();

            Level = new LevelAssetService(prefabPool);
            Ui = new UiAssetService(prefabPool);
        }

        #endregion



        #region IAssetServices

        public LevelAssetService Level { get; }
        public UiAssetService Ui { get; }

        #endregion
    }
}
