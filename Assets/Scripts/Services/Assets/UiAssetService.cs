using FifteenPuzzle.Ui;
using UnityEngine;


namespace FifteenPuzzle.Services.Assets
{
    public class UiAssetService : AssetServiceBase
    {
        public UiAssetService(PrefabPool prefabPool) : base(prefabPool) { }


        public ScreenBase InstantiateScreen(ScreenType screenType, Transform parent) =>
            Instantiate<ScreenBase>(Paths.Ui.GetScreenPath(screenType), parent);

        public PopupBase InstantiatePopup(PopupType popupType, Transform parent) =>
            Instantiate<PopupBase>(Paths.Ui.GetPopupPath(popupType), parent);
    }
}
