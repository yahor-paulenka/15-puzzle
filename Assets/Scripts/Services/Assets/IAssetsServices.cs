namespace FifteenPuzzle.Services.Assets
{
    public interface IAssetsServices
    {
        LevelAssetService Level { get; }
        UiAssetService Ui { get; }
    }
}
