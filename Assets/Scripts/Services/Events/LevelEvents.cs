using System;
using UniRx;


namespace FifteenPuzzle.Services.Events
{
    public class LevelEvents
    {
        private readonly Subject<Unit> levelFinished = new Subject<Unit>();

        public IObservable<Unit> LevelFinished => levelFinished.AsObservable();

        public void LevelFinish() => levelFinished.OnNext(Unit.Default);
    }
}
