namespace FifteenPuzzle.Services.Events
{
    public interface IEventsService
    {
        LevelEvents Level { get; }
    }
}
