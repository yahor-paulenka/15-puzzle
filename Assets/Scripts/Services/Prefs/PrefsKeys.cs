namespace FifteenPuzzle.Services.Prefs
{
    public static class PrefsKeys
    {
        public static class Level
        {
            private const string Prefix = "level_";
            public const string FieldInfo = Prefix + "field_info";
        }
    }
}
