using FifteenPuzzle.Game;
using FifteenPuzzle.Level;
using FifteenPuzzle.Services.Assets;
using FifteenPuzzle.Services.Prefs;
using UnityEngine;


namespace FifteenPuzzle.Services.Level
{
    public class LevelServiceImpl : ILevelService
    {
        #region Fields

        private readonly IAssetsServices assetsServices;
        private readonly IPrefsService prefsService;

        #endregion



        #region Class lifecycle

        public LevelServiceImpl(IAssetsServices assetsServices, IPrefsService prefsService)
        {
            this.assetsServices = assetsServices;
            this.prefsService = prefsService;
        }

        #endregion



        #region ILevelService

        public LevelController LevelController { get; private set; }


        public void CreateLevel(int fieldSize, out bool isNewLevel)
        {
            DisposeActiveLevel();

            GameManager gameManager = GameManager.Instance;
            LevelController = assetsServices.Level.InstantiateLevelController(fieldSize, gameManager.GameRoot);
            gameManager.GameCamera.orthographicSize = 1.25f * LevelController.FieldSize;

            isNewLevel = !LevelController.HasSave;
        }


        public void DisposeActiveLevel()
        {
            if (LevelController == null)
            {
                return;
            }

            Object.Destroy(LevelController.gameObject);
            LevelController = null;
        }

        #endregion
    }
}
