using FifteenPuzzle.Level;


namespace FifteenPuzzle.Services.Level
{
    public interface ILevelService
    {
        LevelController LevelController { get; }
        void CreateLevel(int fieldSize, out bool isNewLevel);
        void DisposeActiveLevel();
    }
}
