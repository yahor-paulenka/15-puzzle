using FifteenPuzzle.Level;


namespace Paths
{
    public static class Cell
    {
        private const string Root = Common.PrefabsRoot + "Cells/";
        private const string PrefixCell = "Cells_";

        public static string GetPath(CellType cellType) => Root + PrefixCell + cellType;
    }
}
