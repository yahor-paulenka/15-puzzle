using FifteenPuzzle.Ui;


namespace Paths
{
    public static class Ui
    {
        private const string Root = Common.PrefabsRoot + "Ui/";

        public const string Manager = Root + "Ui_Manager";

        public static string GetScreenPath(ScreenType screenType) => Root + "Screens/Ui_Screens_" + screenType;
        public static string GetPopupPath(PopupType popupType) => Root + "Popups/Ui_Popups_" + popupType;
    }
}
