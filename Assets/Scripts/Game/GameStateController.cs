using System.Linq;


namespace FifteenPuzzle.Game
{
    public class GameStateController
    {
        #region Fields

        private IState[] states;
        private IState previousState;

        #endregion



        #region Class lifecycle

        public GameStateController()
        {
            states = new IState[]
            {
                new InGameState(),
                new Result(),
                new Restart()
            };
        }

        #endregion



        #region Public methods

        public void SetState(StateType stateType)
        {
            if (previousState != null)
            {
                previousState.OnExit();
            }

            previousState = states.First(s => s.Type == stateType);
            previousState.OnEnter();
        }

        #endregion
    }
}
