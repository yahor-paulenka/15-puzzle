using FifteenPuzzle.Ui;


namespace FifteenPuzzle.Game
{
    public class Result : IState
    {
        #region IState

        public StateType Type => StateType.Result;


        public void OnEnter()
        {
            UiManager.Instance.ShowPopup(PopupType.Result);
        }


        public void OnExit() { }

        #endregion
    }
}
