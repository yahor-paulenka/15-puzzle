namespace FifteenPuzzle.Game
{
    public interface IState
    {
        StateType Type { get; }
        void OnEnter();
        void OnExit();
    }
}
