using FifteenPuzzle.Services;
using FifteenPuzzle.Services.Events;
using FifteenPuzzle.Services.Level;
using FifteenPuzzle.Ui;
using UniRx;


namespace FifteenPuzzle.Game
{
    public class InGameState : IState
    {
        #region Fields

        private readonly ILevelService levelService;
        private readonly IEventsService eventsService;
        private readonly CompositeDisposable disposables;

        #endregion



        #region Class lifecycle

        public InGameState()
        {
            ServicesHub servicesHub = ServicesHub.Instance;
            levelService = servicesHub.Level;
            eventsService = servicesHub.Events;
            disposables = new CompositeDisposable();
        }

        #endregion



        #region IState

        public StateType Type => StateType.InGame;


        public void OnEnter()
        {
            levelService.CreateLevel(4, out bool isNewLevel);
            if (isNewLevel)
            {
                UiManager.Instance.ShowPopup(PopupType.ChoosingFieldSize);
            }

            eventsService.Level.LevelFinished
                .Subscribe(_ => GameManager.Instance.SetGameState(StateType.Result))
                .AddTo(disposables);
        }


        public void OnExit()
        {
            disposables.Clear();
        }

        #endregion
    }
}
