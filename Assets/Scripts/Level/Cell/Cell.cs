using DG.Tweening;
using TMPro;
using UnityEngine;


namespace FifteenPuzzle.Level
{
    [RequireComponent(typeof(BoxCollider))]
    public class Cell : MonoBehaviour
    {
        #region Fields

        [SerializeField] private TextMeshPro cellNumber;

        private BoxCollider boxCollider;
        private int fieldSize;
        private float localPositionOffset;

        #endregion



        #region Properties

        public Vector2 Size => BoxCollider.size;
        public int Number { get; private set; }
        public Vector2Int FieldPosition { get; private set; }


        private BoxCollider BoxCollider
        {
            get
            {
                if (boxCollider == null)
                {
                    boxCollider = GetComponent<BoxCollider>();
                }

                return boxCollider;
            }
        }


        public bool IsRightPosition => Number - 1 == FieldPosition.x + FieldPosition.y * fieldSize;

        #endregion



        #region Public methods

        public void Initialize(int number, int fieldSize, Vector2Int fieldPosition)
        {
            this.fieldSize = fieldSize;
            Number = number;
            cellNumber.text = number.ToString();
            localPositionOffset = (fieldSize - 1.0f) / 2.0f;

            Move(fieldPosition);
        }


        public Tweener Move(Vector2Int fieldPosition, bool isAnimation = false,
            float speedScale = 1.0f)
        {
            FieldPosition = fieldPosition;

            Vector3 targetLocalPosition = transform.localPosition;
            targetLocalPosition.x = (FieldPosition.x * Size.x) - (localPositionOffset * Size.x);
            targetLocalPosition.y = (-FieldPosition.y * Size.y) + (localPositionOffset * Size.y);
            float duration = isAnimation ? 0.4f : 0.0f;
            duration *= speedScale;

            return transform.DOLocalMove(targetLocalPosition, duration)
                .SetEase(Ease.OutQuart)
                .SetLink(gameObject);
        }

        #endregion
    }
}
