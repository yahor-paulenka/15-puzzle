using DG.Tweening;
using FifteenPuzzle.Game;
using FifteenPuzzle.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using FifteenPuzzle.Services.Prefs;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;


namespace FifteenPuzzle.Level
{
    public class LevelController : MonoBehaviour
    {
        #region Field

        private List<Cell> cells;
        private Vector2Int freeCellFieldPosition;
        private RaycastHit[] raycastHits;

        #endregion



        #region Properties

        public int FieldSize { get; private set; }
        public bool HasSave =>  ServicesHub.Instance.Prefs.HasKey(PrefsKeys.Level.FieldInfo);
        private bool IsControlActive { get; set; }

        #endregion



        #region Public methods

        public void Initialize(int fieldSize)
        {
            FieldInfoSave save = null;
            if (HasSave)
            {
                IsControlActive = true;
                save = ServicesHub.Instance.Prefs.GetObjectValue<FieldInfoSave>(PrefsKeys.Level.FieldInfo);
                if (save == null)
                {
                    throw new NullReferenceException();
                }
            }

            cells = CreateCells(fieldSize, save);

            raycastHits = new RaycastHit[10];

            var clickMouseUpStream = Observable.EveryUpdate().Where(_ => Input.GetMouseButtonUp(0));

            Observable.EveryUpdate()
                .Where(_ => IsControlActive)
                .Where(_ => Input.GetMouseButtonDown(0))
                .Select(_ =>
                {
                    Ray ray = GameManager.Instance.GameCamera.ScreenPointToRay(Input.mousePosition);
                    int hits = Physics.RaycastNonAlloc(ray, raycastHits, 50.0f);
                    return raycastHits
                        .Take(hits)
                        .Select(rh => rh.transform.GetComponent<Cell>())
                        .FirstOrDefault(c => c != null);
                })
                .Where(cell => cell != null)
                .Where(cell => cell.FieldPosition.x == freeCellFieldPosition.x ||
                    cell.FieldPosition.y == freeCellFieldPosition.y)
                .Buffer(clickMouseUpStream)
                .Where(lc => lc.Count > 0)
                .Select(c => c.First().FieldPosition)
                .Subscribe(cell =>
                {
                    MoveCells(cell);
                    CheckVictory();
                })
                .AddTo(this);
        }


        private List<Cell> CreateCells(int fieldSize, FieldInfoSave save)
        {
            FieldSize = HasSave ? save.fieldSize : fieldSize;

            int cellCount = FieldSize * FieldSize - 1;
            string cellPath = Paths.Cell.GetPath(CellType.Default);
            List<Cell> result = ServicesHub.Instance.Assets.Level.CreatCells(cellCount, cellPath, transform);

            Vector2Int fieldPosition = new Vector2Int(0, 0);
            for (int i = 0; i < result.Count; i++)
            {
                Cell cell = result[i];

                int number = i + 1;
                if (HasSave)
                {
                    fieldPosition = save.cells[number];
                }

                cell.Initialize(number, FieldSize, fieldPosition);

                fieldPosition.x++;
                if (fieldPosition.x == FieldSize)
                {
                    fieldPosition.x = 0;
                    fieldPosition.y++;
                }
            }

            freeCellFieldPosition = HasSave ? save.freeCellFieldPosition : fieldPosition;

            return result;
        }


        public void MixCells()
        {
            IsControlActive = false;

            List<Vector2Int> positions = new List<Vector2Int> {GetRandomPositions(FieldSize, freeCellFieldPosition)};
            for (int i = 0; i < 5 * FieldSize; i++)
            {
                int secondToLastIndex = positions.Count - 2;
                Vector2Int[] ignorePositions = secondToLastIndex < 0 ?
                    null :
                    new[] {positions[secondToLastIndex]};

                positions.Add(GetRandomPositions(FieldSize, positions.Last(),
                    ignorePositions));
            }

            Sequence randomCellsSequence = DOTween.Sequence().SetLink(gameObject);
            for (int i = 0; i < positions.Count; i++)
            {
                randomCellsSequence.Append(MoveCells(positions[i], 0.4f));
            }

            randomCellsSequence.OnComplete(() => IsControlActive = true);
        }

        #endregion



        #region Private methods

        private Sequence MoveCells(Vector2Int targetFreeCellPosition, float speedScale = 1.0f)
        {
            int countSteps;
            Vector2Int step = new Vector2Int(0, 0);
            if (targetFreeCellPosition.x == freeCellFieldPosition.x)
            {
                countSteps = freeCellFieldPosition.y - targetFreeCellPosition.y;
                step.y = countSteps > 0 ? -1 : 1;
            }
            else
            {
                countSteps = freeCellFieldPosition.x - targetFreeCellPosition.x;
                step.x = countSteps > 0 ? -1 : 1;
            }

            Sequence moveSequence = DOTween.Sequence().SetLink(gameObject);
            for (int i = 0; i < Mathf.Abs(countSteps); i++)
            {
                Vector2Int newPosition = freeCellFieldPosition + step;
                Cell targetCell = cells.First(c => c.FieldPosition == newPosition);
                moveSequence.Join(targetCell.Move(freeCellFieldPosition, true, speedScale));

                freeCellFieldPosition = newPosition;
            }

            return moveSequence;
        }


        private Vector2Int GetRandomPositions(int fieldSize, Vector2Int freePosition,
            Vector2Int[] ignorePositions = null)
        {
            List<Vector2Int> canMovePositions = new List<Vector2Int>();
            Vector2Int fieldPosition = freePosition;
            fieldPosition.y = 0;
            for (int i = 0; i < fieldSize - 1; i++)
            {
                if (fieldPosition == freePosition)
                {
                    fieldPosition.y++;
                }

                canMovePositions.Add(fieldPosition);

                fieldPosition.y++;
            }

            fieldPosition = freePosition;
            fieldPosition.x = 0;
            for (int i = 0; i < fieldSize - 1; i++)
            {
                if (fieldPosition == freePosition)
                {
                    fieldPosition.x++;
                }

                canMovePositions.Add(fieldPosition);

                fieldPosition.x++;
            }

            if (ignorePositions != null)
            {
                for (int i = 0; i < ignorePositions.Length; i++)
                {
                    canMovePositions.Remove(ignorePositions[i]);
                }
            }

            int randomIndex = Random.Range(0, canMovePositions.Count);
            return canMovePositions[randomIndex];
        }


        private void CheckVictory()
        {
            if (cells.All(c => c.IsRightPosition))
            {
                IsControlActive = false;
                ServicesHub servicesHub  = ServicesHub.Instance;
                servicesHub.Prefs.DeleteKey(PrefsKeys.Level.FieldInfo);
                servicesHub.Events.Level.LevelFinish();
            }
            else
            {
                Save();
            }
        }


        private void Save()
        {
            FieldInfoSave save = new FieldInfoSave
            {
                fieldSize = FieldSize,
                freeCellFieldPosition =  freeCellFieldPosition,
                cells = cells.ToDictionary(c => c.Number, c => c.FieldPosition)
            };

            ServicesHub.Instance.Prefs.SetObjectValue(PrefsKeys.Level.FieldInfo, save, true);
        }

        #endregion
    }
}
