using System;
using System.Collections.Generic;
using UnityEngine;


namespace FifteenPuzzle.Level
{
    [Serializable]
    public class FieldInfoSave
    {
        public int fieldSize;
        public Vector2Int freeCellFieldPosition;
        public Dictionary<int, Vector2Int> cells;
    }
}
