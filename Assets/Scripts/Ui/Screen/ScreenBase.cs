using UnityEngine;


namespace FifteenPuzzle.Ui
{
    public class ScreenBase : MonoBehaviour
    {
        public virtual void OnHide()
        {
            Destroy(gameObject);
        }
    }
}
