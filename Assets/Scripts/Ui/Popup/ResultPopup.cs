using FifteenPuzzle.Game;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace FifteenPuzzle.Ui
{
    public class ResultPopup : PopupBase
    {
        #region Fields

        [SerializeField] private Button newGameButton;

        #endregion



        #region Unity lifecycle

        protected override void Awake()
        {
            base.Awake();

            Observable.FromEvent(h => new UnityAction(h),
                    h => newGameButton.onClick.AddListener(h),
                    h => newGameButton.onClick.RemoveListener(h))
                .Subscribe(_ =>
                {
                    GameManager.Instance.SetGameState(StateType.Restart);
                    OnClose();
                })
                .AddTo(gameObject);
        }

        #endregion
    }
}
