using System;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace FifteenPuzzle.Ui
{
    public class PopupBase : MonoBehaviour
    {
        #region Fields

        [SerializeField] private Button[] closeButtons;

        #endregion



        #region Unity lifecycle

        protected virtual void Awake()
        {
            for (int i = 0; i < closeButtons.Length; i++)
            {
                Button button = closeButtons[i];

                Observable.FromEvent(h => new UnityAction(h),
                        h => button.onClick.AddListener(h),
                        h => button.onClick.RemoveListener(h))
                    .Subscribe(_ => OnClose())
                    .AddTo(gameObject);
            }
        }

        #endregion



        #region Protected methods

        protected void OnClose()
        {
            UiManager.Instance.DestroyPopup(this);
        }

        #endregion
    }
}
