using FifteenPuzzle.Services;
using FifteenPuzzle.Services.Level;
using UniRx;
using UnityEngine;


namespace FifteenPuzzle.Ui
{
    public class ChoosingFieldSizePopup : PopupBase
    {
        #region Fields

        [SerializeField] private RectTransform fieldSizeButtonsRoot;

        #endregion



        #region Unity lifecycle

        protected override void Awake()
        {
            base.Awake();

            ServicesHub servicesHub = ServicesHub.Instance;
            ILevelService levelService = servicesHub.Level;

            for (int i = 0; i < fieldSizeButtonsRoot.childCount; i++)
            {
                FieldSizeButton button = fieldSizeButtonsRoot.GetChild(i)
                    .GetComponent<FieldSizeButton>();
                if (button == null)
                {
                    continue;
                }

                button.OnClick.Subscribe(size =>
                    {
                        if (levelService.LevelController.FieldSize != size)
                        {
                            levelService.CreateLevel(size, out _);
                        }

                        levelService.LevelController.MixCells();
                        OnClose();
                    })
                    .AddTo(this);
            }
        }

        #endregion
    }
}
