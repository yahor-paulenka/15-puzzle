using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace FifteenPuzzle.Ui
{
    public class FieldSizeButton : MonoBehaviour
    {
        #region Fields

        private readonly Subject<int> onClick = new Subject<int>();

        [SerializeField] private Button button;
        [SerializeField] [Range(0, 5)] private int size;
        [SerializeField] private TextMeshProUGUI sizeLabel;

        #endregion



        #region Properties

        public IObservable<int> OnClick => onClick.AsObservable();

        #endregion



        #region Unity lifecycle

        private void Awake()
        {
            sizeLabel.text = $"{size} x {size}";

            Observable.FromEvent(h => new UnityAction(h),
                    h => button.onClick.AddListener(h),
                    h => button.onClick.RemoveListener(h))
                .Subscribe(_ => onClick.OnNext(size))
                .AddTo(gameObject);
        }

        #endregion
    }
}
