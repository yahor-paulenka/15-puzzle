namespace FifteenPuzzle.Ui
{
    public enum PopupType
    {
        None = 0,
        ChoosingFieldSize = 1,
        Result = 2,
    }
}
