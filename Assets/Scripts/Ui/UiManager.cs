using System.Collections.Generic;
using FifteenPuzzle.Services;
using FifteenPuzzle.Services.Assets;
using UnityEngine;


namespace FifteenPuzzle.Ui
{
    public class UiManager : SingletonMonoBehaviour<UiManager>
    {
        #region Fields

        [SerializeField] private RectTransform screensRoot;
        [SerializeField] private RectTransform popupsRoot;

        private IAssetsServices assetsServices;
        private List<PopupBase> currentPopups;
        private ScreenBase previousScreen;

        #endregion



        #region Unity lifecycle

        private void Awake()
        {
            DontDestroyOnLoad(this);

            ServicesHub servicesHub = ServicesHub.Instance;
            assetsServices = servicesHub.Assets;
            currentPopups = new List<PopupBase>();
        }

        #endregion



        #region Public merhods

        public ScreenBase ShowScreen(ScreenType screenType)
        {
            if (previousScreen != null)
            {
                previousScreen.OnHide();
            }

            previousScreen = assetsServices.Ui.InstantiateScreen(screenType, screensRoot);
            return previousScreen;
        }


        public PopupBase ShowPopup(PopupType popupType)
        {
            PopupBase popup = assetsServices.Ui.InstantiatePopup(popupType, popupsRoot);
            currentPopups.Add(popup);
            return popup;
        }


        public void DestroyPopup(PopupBase popupToDestroy)
        {
            currentPopups.Remove(popupToDestroy);
            Destroy(popupToDestroy.gameObject);
        }

        #endregion
    }
}
